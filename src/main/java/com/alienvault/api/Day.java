package com.alienvault.api;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Day {
    private String day;
    private Long total;
    private Map<String, Long> occurrences;

    public Day() {
        this.day = "";
        this.total = 0L;
        this.occurrences = new HashMap<>();
    }

    public Day(String day, List<String> repo) {
        this.day = day;
        this.total = 0L;
        this.occurrences = new HashMap<>();
        for (String r : repo) {
            this.occurrences.put(r, 0L);
        }
    }

    public String getDay() {
        return day;
    }

    public Map<String, Long> getOccurrences() {
        return occurrences;
    }

    @JsonIgnore
    public Long getTotal() {
        return total;
    }

    public void add(String repo) {
        total++;
        long value = occurrences.get(repo);
        value++;
        occurrences.put(repo, value);
    }
}
