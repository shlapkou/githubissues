package com.alienvault.api;

import java.util.List;

/**
 * Class manage git hub statics
 */
public interface StatisticManager {
    /**
     * Get statistics about all issues.
     *
     * @return list of issues
     */
    List<Issue> getIssues();

    /**
     * Get top day
     *
     * @return return top day
     */
    Day getTopDay();

    /**
     * Output statistics in json format.
     *
     * @return statistics as a string in json representations.
     */
    String toJsonString() throws StatisticException;

    /**
     * Add new issue
     * @param issue
     */
    void addIssue(Issue issue);

    /**
     * Clean statistics before processing all issues.
     * @param repositories
     */
    void clean(List<String> repositories);

    /**
     * Calculate statistics after finish processing all issues.
     */
    void calculate();
}
