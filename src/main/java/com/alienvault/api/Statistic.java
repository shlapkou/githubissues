package com.alienvault.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Class represent git hub statics
 */
public final class Statistic {
    private List<Issue> issues;
    private Map<String, Day> days;
    private List<String> repositories;
    private Day topDay;

    public Statistic()
    {
        this.issues = new LinkedList<>();
        this.days = new HashMap<>();
        this.topDay = new Day();
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }

    @JsonProperty("top_day")
    public Day getTopDay() {
        return topDay;
    }

    @JsonProperty("top_day")
    public void setTopDay(Day day) {
        this.topDay = day;
    }
    @JsonIgnore
    public Map<String, Day> getDays() {
        return days;
    }

    @JsonIgnore
    public void setDays(Map<String, Day> days) {
        this.days = days;
    }

    @JsonIgnore
    public List<String> getRepositories() {
        return repositories;
    }

    @JsonIgnore
    public void setRepositories(List<String> repositories) {
        this.repositories = repositories;
    }
}
