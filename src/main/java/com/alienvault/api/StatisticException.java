package com.alienvault.api;

public class StatisticException extends Exception {

    public StatisticException(String message) {
        super(message);
    }

    public StatisticException(Exception e) {
        super(e);
    }
}
