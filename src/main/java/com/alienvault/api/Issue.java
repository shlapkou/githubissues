package com.alienvault.api;

public final class Issue {
    private int id;
    private String state;
    private String title;
    private String repository;
    private String created;

    public Issue() {
    }

    public Issue(int id, String state, String title, String repository, String created) {
        this.id = id;
        this.state = state;
        this.title = title;
        this.repository = repository;
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public String getTitle() {
        return title;
    }

    public String getRepository() {
        return repository;
    }

    public String getCreated() {
        return created;
    }


}
