package com.alienvault.api;

import java.util.List;

/**
 * Interface represent github connections.
 */
public interface GitHubManager {

    /**
     * Anonymous access to Github.
     *
     * @param repositories - list of repositories in format ""owner/repository"
     */
    void connect(List<String> repositories);

    /**
     * Connect with HTTP Basic Authentication.
     *
     * @param userName     - github user name
     * @param password     - github password
     * @param repositories - list of repositories in format ""owner/repository"
     */
    void connect(String userName, String password, List<String> repositories);

    void refresh();

    StatisticManager getStatistics();

}
