package com.alienvault.github;

import com.alienvault.api.GitHubManager;
import com.alienvault.api.StatisticManager;
import com.alienvault.statistics.StatisticsManagerImpl;
import com.jcabi.github.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.json.JsonObject;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Github manager implementations with Object-Oriented Github API based v3:
 * https://github.jcabi.com/index.html
 */
public class GitHubManagerImpl implements GitHubManager {
    private static final Log log = LogFactory.getLog(GitHubManagerImpl.class);
    private static final Map<String, String> properties = new HashMap<>();

    static {
        properties.put("state", "all");
        properties.put("filter", "all");
    }

    private Github github;
    private List<String> repositories;
    private StatisticsManagerImpl statistics;

    public GitHubManagerImpl() {
        statistics = new StatisticsManagerImpl();
    }

    @Override
    public void connect(List<String> repositories) {
        this.github = new RtGithub();
        this.repositories = repositories;
    }

    @Override
    public void connect(String userName, String password, List<String> repositories) {
        this.github = new RtGithub(userName, password);
        this.repositories = repositories;
    }

    @Override
    public void refresh() {
        try {
            log.debug("Refresh statistics");
            statistics.clean(repositories);
            for (String repoName : repositories) {
                log.debug("Get issues for repo " + repoName);
                Coordinates coords = new Coordinates.Simple(repoName);
                Repo repo = github.repos().get(coords);
                for (Issue issueGit : repo.issues().iterate(properties)) {
                    log.debug("Issues  " + issueGit.number());
                    JsonObject issueJson = issueGit.json();
                    com.alienvault.api.Issue issue = new com.alienvault.api.Issue(issueGit.number(), issueJson.getString("state"),
                            issueJson.getString("title"), repoName, issueJson.getString("created_at"));
                    statistics.addIssue(issue);
                }
            }
            statistics.calculate();
        } catch (IOException e) {
            log.error("Error to get statistics from git hub " + e.getMessage());
        }

    }

    @Override
    public StatisticManager getStatistics() {
        return statistics;
    }
}
