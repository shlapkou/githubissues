package com.alienvault.statistics;

import com.alienvault.api.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import java.util.*;

public class StatisticsManagerImpl implements StatisticManager {
    private static final Log log = LogFactory.getLog(StatisticsManagerImpl.class);
    private Statistic statistic;
    private ObjectMapper objectMapper;

    public StatisticsManagerImpl() {
        this.statistic = new Statistic();
        this.objectMapper = new ObjectMapper();
        this.objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        this.objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    @Override
    public List<Issue> getIssues() {
        return statistic.getIssues();
    }

    @Override
    @JsonProperty("top_day")
    public Day getTopDay() {
        return this.statistic.getTopDay();
    }

    @Override
    public String toJsonString() throws StatisticException {
        try {

            return objectMapper.writeValueAsString(statistic);
        } catch (JsonProcessingException e) {
            log.error("Error to create json representations for statistics " + e.getMessage());
            throw new StatisticException(e);
        }
    }

    @Override
    public void clean(List<String> repositories) {
        this.statistic.setRepositories(repositories);
        this.statistic.setIssues(new LinkedList<>());
        this.statistic.setDays(new HashMap<>());
    }

    @Override
    public void calculate() {
        statistic.getIssues().sort(Comparator.comparing(Issue::getCreated));
        statistic.setTopDay(statistic.getDays().values().stream().max((d1, d2) -> {
            if (d1.getTotal().equals(d2.getTotal())) {
                return d1.getDay().compareTo(d2.getDay());
            } else {
                return d1.getTotal().compareTo(d2.getTotal());
            }
        }).get());
    }

    public void addIssue(Issue issue) {
        String date = issue.getCreated().substring(0, 10);
        if (statistic.getDays().containsKey(date)) {
            statistic.getDays().get(date).add(issue.getRepository());
        } else {
            Day day = new Day(date, statistic.getRepositories());
            day.add(issue.getRepository());
            statistic.getDays().put(date, day);
        }
        statistic.getIssues().add(issue);
    }
}
