package com.alienvault;

import com.alienvault.api.GitHubManager;
import com.alienvault.api.StatisticException;
import com.alienvault.github.GitHubManagerImpl;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    static Path outputFile = Paths.get("output.txt");

    public static void main(String[] args) {
      if(args.length == 0) {
        System.out.println("Incorrect usage please provide input file and/or github username password with command line arguments");
        return;
      }
      try {
          System.out.println("Starting retrying issues from github ... ");
          Path inputFile = Paths.get(args[0]);
          boolean useBasicAuth = (args.length == 3);
          List<String> repos = Files.readAllLines(inputFile);
          GitHubManager manager = new GitHubManagerImpl();
          if(useBasicAuth) {
              manager.connect(args[1], args[2], repos);
          }
          else {
              manager.connect(repos);
          }
          manager.refresh();
          System.out.println("Writing results to output.txt ...");
          Files.write(outputFile, manager.getStatistics().toJsonString().getBytes(),
                  StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
          System.out.println("DONE.");
      }
      catch (Exception e) {
          System.err.println("Error access github " + e.getMessage());
      }
    }

}
