package com.alienvault.statistics;


import com.alienvault.api.Day;
import com.alienvault.api.Issue;
import com.alienvault.api.StatisticException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StatisticsManagerImplTest {
    @Test
    public void testStatisticsImpl() {
        StatisticsManagerImpl statistics = new StatisticsManagerImpl();
        List<String> repo = new ArrayList<>();
        repo.add("owner1/repository1");
        repo.add("owner2/repository2");
        statistics.clean(repo);

        statistics.addIssue(new Issue(23, "open", "Found a bug 2", "owner1/repository1", "2011-04-22T18:24:32Z"));
        statistics.addIssue(new Issue(24, "closed", "Feature request", "owner2/repository2", "2011-05-08T09:15:20Z"));
        statistics.addIssue(new Issue(38, "open", "Found a bug", "owner1/repository1", "2011-04-22T13:33:48Z"));

        statistics.calculate();

        assertEquals(3, statistics.getIssues().size());
        Day topDay = statistics.getTopDay();

        assertEquals(2, topDay.getOccurrences().size());
        assertEquals(2, topDay.getOccurrences().get("owner1/repository1").longValue());
        assertEquals("2011-04-22", topDay.getDay());
    }

    @Test
    public void testJsonPrint() throws StatisticException {
        StatisticsManagerImpl statistics = new StatisticsManagerImpl();
        List<String> repo = new ArrayList<>();
        repo.add("owner1/repository1");
        repo.add("owner2/repository2");
        statistics.clean(repo);

        statistics.addIssue(new Issue(23, "open", "Found a bug 2", "owner1/repository1", "2011-04-22T18:24:32Z"));
        statistics.addIssue(new Issue(24, "closed", "Feature request", "owner2/repository2", "2011-05-08T09:15:20Z"));
        statistics.addIssue(new Issue(38, "open", "Found a bug", "owner1/repository1", "2011-04-22T13:33:48Z"));

        statistics.calculate();
        String expected = "{\r\n" +
                "  \"issues\" : [ {\r\n" +
                "    \"id\" : 38,\r\n" +
                "    \"state\" : \"open\",\r\n" +
                "    \"title\" : \"Found a bug\",\r\n" +
                "    \"repository\" : \"owner1/repository1\",\r\n" +
                "    \"created\" : \"2011-04-22T13:33:48Z\"\r\n" +
                "  }, {\r\n" +
                "    \"id\" : 23,\r\n" +
                "    \"state\" : \"open\",\r\n" +
                "    \"title\" : \"Found a bug 2\",\r\n" +
                "    \"repository\" : \"owner1/repository1\",\r\n" +
                "    \"created\" : \"2011-04-22T18:24:32Z\"\r\n" +
                "  }, {\r\n" +
                "    \"id\" : 24,\r\n" +
                "    \"state\" : \"closed\",\r\n" +
                "    \"title\" : \"Feature request\",\r\n" +
                "    \"repository\" : \"owner2/repository2\",\r\n" +
                "    \"created\" : \"2011-05-08T09:15:20Z\"\r\n" +
                "  } ],\r\n" +
                "  \"top_day\" : {\r\n" +
                "    \"day\" : \"2011-04-22\",\r\n" +
                "    \"occurrences\" : {\r\n" +
                "      \"owner1/repository1\" : 2,\r\n" +
                "      \"owner2/repository2\" : 0\r\n" +
                "    }\r\n" +
                "  }\r\n" +
                "}";

        assertEquals(expected, statistics.toJsonString());
    }
}
