### AlienVault Mini Project
#### What you’ll need
JDK 1.8 or later
Maven 3.2+
#### Build the project:

`mvn clean install`

#### Start application with anonymous access to github:

`java -jar ./target/githubissues-1.0-SNAPSHOT.jar input.txt`

#### Start application with basic auth username/password access to github:

`java -jar ./target/githubissues-1.0-SNAPSHOT.jar input.txt username password`

#### Input file format a list of 1 to n Strings with Github repositories references with the format "owner/repository":

neo4j/neo4j-javascript-driver

#### You can find result json in output.txt